# Created by newuser for 5.2

## Command history configuration
if [ -z "$HISTFILE" ]; then
    HISTFILE=$HOME/.zsh_history
fi

HISTSIZE=10000
SAVEHIST=10000

# Show history
case $HIST_STAMPS in
  "mm/dd/yyyy") alias history='fc -fl 1' ;;
  "dd.mm.yyyy") alias history='fc -El 1' ;;
  "yyyy-mm-dd") alias history='fc -il 1' ;;
  *) alias history='fc -l 1' ;;
esac

setopt append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups # ignore duplication command history list
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history # share command history data

# Useful
export EDITOR="vim"

# Auto completion
autoload -U promptinit zcalc zsh-mime-setup
promptinit
zsh-mime-setup
zstyle ':completion::complete:*' use-cache 1

bindkey -v
bindkey "^R" history-incremental-pattern-search-backward

# Silent
setopt NO_BEEP
setopt PUSHD_SILENT

# Initialise Docker machine env
docker-machine start default
eval "$(docker-machine env default)"

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Virtualenv Wrapper
source /usr/local/bin/virtualenvwrapper.sh
export PATH="/usr/local/sbin:$PATH"

# Set Bake env. variables
export BAKE_HOME=/Users/laurence/bin/bake 
export PATH=$PATH:$BAKE_HOME
export PYTHONPATH=$PYTHONPATH:$BAKE_HOME
