" Line numbers
set nu!

" Appearance
colorscheme elflord
if $TERM == "xterm-256color"
  set t_Co=256
endif
highlight Normal guifg=white 

" Syntax
syntax on
au BufReadPost *.sample set syntax=json
autocmd FileType make setlocal noexpandtab
autocmd FileType styl setlocal shiftwidth=2 
autocmd FileType styl setlocal tabstop=2

" Tab properties
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab

" Tab shortcuts
nnoremap th :tabfirst<CR>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprev<CR>
nnoremap tl :tablast<CR>
nnoremap tt :tabedit<CR>
nnoremap tm :tabm<CR>
nnoremap td :tabclose<CR>
nnoremap tn :tabnew<CR>
